<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Model\Request;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

abstract class AbstractListRequestModel
{
    /**
     * @var int
     * @Assert\Range(min=1, max=1000)
     * @JMS\Type("int")
     */
    protected $page = 1;

    /**
     * @var int
     * @Assert\Range(min=1, max=100)
     * @JMS\Type("int")
     */
    protected $limit = 10;

    /**
     * @var string
     * @JMS\Type("string")
     */
    protected $sort;

    /**
     * @var string
     * @Assert\Choice(choices={"asc", "desc"})
     * @JMS\Type("string")
     */
    protected $sortType = 'asc';

    /**
     * @var array
     * @JMS\Type("array<string, string>")
     */
    protected $filters = [];

    public function getPage(): int
    {
        return $this->page;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getSort(): string
    {
        return $this->sort;
    }

    public function getSortType(): string
    {
        return $this->sortType;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }
}
