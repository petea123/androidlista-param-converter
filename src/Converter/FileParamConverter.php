<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Converter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileParamConverter implements ParamConverterInterface
{
    public const CONVERTER_NAME = 'androidlista.file';

    /** @inheritdoc */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = $configuration->getOptions();
        $name = \is_array($options) && \array_key_exists('file_name', $options) ? $options['file_name'] : $configuration->getName();

        /** @var UploadedFile $file */
        $file = $request->files->get($name);

        if (!$file) {
            throw new BadRequestHttpException("File {$name} is required");
        }

        if (!$file->isValid()) {
            throw new BadRequestHttpException($file->getErrorMessage());
        }

        $request->attributes->set($configuration->getName(), $file);
    }

    /** @inheritdoc */
    public function supports(ParamConverter $configuration)
    {
        return ($configuration->getClass() === UploadedFile::class)
            && $configuration->getConverter() === self::CONVERTER_NAME;
    }
}
