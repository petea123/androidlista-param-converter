<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Converter;

use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Androidlista\ParamConverterBundle\Exception\ParamConverterValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestParamConverter implements ParamConverterInterface
{
    public const CONVERTER_NAME = 'androidlista.request_dto';
    private const VIOLATIONS_PARAM_NAME = 'violationsList';

    /** @var SerializerInterface */
    private $serializer;
    /** @var ValidatorInterface */
    private $validator;

    /**
     * RequestParamConverter constructor.
     *
     * @param SerializerInterface $serializer
     * @param ValidatorInterface  $validator
     */
    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $dto = $this->serializer->fromArray($this->getData($request), $configuration->getClass());
        $request->attributes->set($configuration->getName(), $dto);

        $options = $configuration->getOptions();

        $errors = $this->validator->validate($dto, null, $options['validator_groups'] ?? null);
        if ($errors->count()) {
            if ($options['throw_exception'] ?? true) {
                throw new ParamConverterValidationException($errors);
            } else {
                $request->attributes->set(self::VIOLATIONS_PARAM_NAME, $errors);
            }
        }
    }

    protected function getData(Request $request): array
    {
        return array_merge($request->request->all(), $request->query->all());
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() && ($configuration->getConverter() === self::CONVERTER_NAME);
    }
}

