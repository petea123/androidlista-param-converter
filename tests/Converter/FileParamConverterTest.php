<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter;

use PHPUnit\Framework\TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Androidlista\ParamConverterBundle\Converter\FileParamConverter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileParamConverterTest extends TestCase
{
    public function supportsProvider(): array
    {
        return [
            [null, null, false],
            ['RandomClass', FileParamConverter::CONVERTER_NAME, false],
            [UploadedFile::class, 'iddqd', false],
            [UploadedFile::class, FileParamConverter::CONVERTER_NAME, true],
        ];
    }

    /**
     * @dataProvider supportsProvider
     * @param string $class
     * @param string $converter
     * @param bool $expectation
     */
    public function testSupports($class, $converter, bool $expectation)
    {
        /** @var ParamConverter|MockObject $config */
        $config = $this->createMock(ParamConverter::class);
        $config
            ->expects($this->any())
            ->method('getClass')
            ->willReturn($class);

        $config
            ->expects($this->any())
            ->method('getConverter')
            ->willReturn($converter);

        $subject = $this->getSubject();

        $this->assertEquals(
            $expectation,
            $subject->supports($config)
        );
    }

    public function testItAddsUploadedFileToTheRequestAttributes(): void
    {
        $name = 'myFile';

        /** @var ParamConverter|MockObject $config */
        $config = $this->createMock(ParamConverter::class);
        $config
            ->expects($this->exactly(2))
            ->method('getName')
            ->willReturn($name);

        /** @var UploadedFile|MockObject $files */
        $file = $this->createMock(UploadedFile::class);
        $file
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        /** @var ParameterBag|MockObject $files */
        $files = $this->createMock(ParameterBag::class);
        $files
            ->expects($this->once())
            ->method('get')
            ->with($name)
            ->willReturn($file);

        /** @var ParameterBag|MockObject $attributes */
        $attributes = $this->createMock(ParameterBag::class);
        $attributes
            ->expects($this->once())
            ->method('set')
            ->with($name, $file);

        $request = new Request();
        $request->attributes = $attributes;
        $request->files      = $files;

        ($this->getSubject())->apply($request, $config);
    }

    public function testItNullifyRequestAttributesIfFileWasNotUploaded(): void
    {
        $name = 'myFile';

        /** @var ParamConverter|MockObject $config */
        $config = $this->createMock(ParamConverter::class);
        $config
            ->expects($this->once())
            ->method('getName')
            ->willReturn($name);

        /** @var ParameterBag|MockObject $files */
        $files = $this->createMock(ParameterBag::class);
        $files
            ->expects($this->once())
            ->method('get')
            ->with($name)
            ->willReturn(null);

        $request = new Request();
        $request->attributes = $attributes;
        $request->files      = $files;

        $this->expectException(BadRequestHttpException::class);

        ($this->getSubject())->apply($request, $config);
    }

    public function testItThrowsExceptionIfFileIsNotValid(): void
    {
        $this->expectException(BadRequestHttpException::class);

        $name = 'myFile';

        /** @var ParamConverter|MockObject $config */
        $config = $this->createMock(ParamConverter::class);
        $config
            ->expects($this->once())
            ->method('getName')
            ->willReturn($name);

        /** @var UploadedFile|MockObject $files */
        $file = $this->createMock(UploadedFile::class);
        $file
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(false);

        /** @var ParameterBag|MockObject $files */
        $files = $this->createMock(ParameterBag::class);
        $files
            ->expects($this->once())
            ->method('get')
            ->with($name)
            ->willReturn($file);

        $request        = new Request();
        $request->files = $files;

        ($this->getSubject())->apply($request, $config);
    }

    private function getSubject(): FileParamConverter
    {
        return new FileParamConverter();
    }
}
