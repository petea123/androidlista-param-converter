<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Androidlista\ParamConverterBundle\Converter\RequestParamConverter;
use Androidlista\ParamConverterBundle\Exception\ParamConverterValidationException;
use Androidlista\ParamConverterBundle\Tests\Converter\Model\QueryRequestModel;
use Androidlista\ParamConverterBundle\Tests\Converter\Model\ValidatableModel;
use Androidlista\ParamConverterBundle\Tests\Converter\Model\ValidatableWithGroupsModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestParamConverterTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;
    /** @var ValidatorInterface */
    private $validator;
    /** @var RequestParamConverter */
    private $paramConverter;

    public function setUp(): void
    {
        AnnotationRegistry::registerLoader('class_exists');
        $this->serializer = SerializerBuilder::create()
            ->setAnnotationReader(new AnnotationReader())
            ->build();
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
        $this->paramConverter = new RequestParamConverter($this->serializer, $this->validator);
    }

    /**
     * @param null|string $className
     * @param null|string $converter
     * @param bool        $expects
     *
     * @dataProvider supportsDataProvider
     */
    public function testSupports(?string $className, ?string $converter, bool $expects)
    {
        $configuration = new ParamConverter(['class' => $className, 'converter' => $converter]);

        $this->assertEquals($expects, $this->paramConverter->supports($configuration));
    }

    public function supportsDataProvider(): array
    {
        return [
            [null, null, false],
            ['', null, false],
            ['class', null, false],

            [null, 'androidlista.request_dto', false],
            ['', 'androidlista.request_dto', false],
            ['class', 'androidlista.request_dto', true],

            ['class', 'some', false],
        ];
    }

    public function testApplyFromGetAndPostInTheSameTime()
    {
        $request = new Request(
            ['common' => 'get', 'query' => 'get'],
            ['common' => 'post', 'request' => 'post']
        );

        $config = new ParamConverter([
            'class' => QueryRequestModel::class,
            'name' => 'test',
            'converter' => 'androidlista.request_dto'
        ]);

        $this->paramConverter->apply($request, $config);
        /** @var QueryRequestModel $resultModel */
        $resultModel = $request->attributes->get('test');

        $this->assertEquals('get', $resultModel->common);
        $this->assertEquals('get', $resultModel->query);
        $this->assertEquals('post', $resultModel->request);
    }

    public function testApplyValidatorException()
    {
        $request = new Request(['name' => '']);

        $config = new ParamConverter([
            'class' => ValidatableModel::class,
            'name' => 'test',
            'converter' => 'androidlista.request_dto'
        ]);

        $this->expectException(ParamConverterValidationException::class);
        $this->paramConverter->apply($request, $config);
    }

    public function testApplyValidatorErrorsAsAttribute()
    {
        $request = new Request(['name' => '']);

        $config = new ParamConverter([
            'class' => ValidatableModel::class,
            'name' => 'test',
            'converter' => 'androidlista.request_dto',
            'options' => ['throw_exception' => false]
        ]);

        $this->paramConverter->apply($request, $config);

        $this->assertInstanceOf(ConstraintViolationList::class, $request->attributes->get('violationsList'));
        $this->assertEquals(1, count($request->attributes->get('violationsList')));
        $this->assertStringNotMatchesFormat('not blank', (string) $request->attributes->get('violationsList'));
    }

    public function testApplyValidatorSingleGroup()
    {
        $request = new Request(['name' => '', 'email' => '']);

        $config = new ParamConverter([
            'class' => ValidatableWithGroupsModel::class,
            'name' => 'test',
            'converter' => 'androidlista.request_dto',
            'options' => ['throw_exception' => false, 'validator_groups' => ['group1']]
        ]);

        $this->paramConverter->apply($request, $config);

        $this->assertEquals(1, count($request->attributes->get('violationsList')));
        $this->assertStringNotMatchesFormat('name', (string) $request->attributes->get('violationsList'));
    }

    public function testApplyValidatorMultyGroups()
    {
        $request = new Request(['name' => '', 'email' => '']);

        $config = new ParamConverter([
            'class' => ValidatableWithGroupsModel::class,
            'name' => 'test',
            'converter' => 'androidlista.request_dto',
            'options' => ['throw_exception' => false, 'validator_groups' => ['group1', 'group2']]
        ]);

        $this->paramConverter->apply($request, $config);

        $this->assertEquals(2, count($request->attributes->get('violationsList')));
    }
}
