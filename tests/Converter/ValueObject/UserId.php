<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter\ValueObject;

class UserId
{
    /** @var int */
    public $id;

    public static function existing(int $id)
    {
        return new self($id);
    }

    private function __construct($id)
    {
        $this->id = $id;
    }

    public function value()
    {
        return $this->id;
    }
}
