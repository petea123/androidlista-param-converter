<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter;

use PHPUnit\Framework\TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Androidlista\ParamConverterBundle\Converter\ValueObjectParamConverter;
use Androidlista\ParamConverterBundle\Tests\Converter\ValueObject\UserId;
use Symfony\Component\HttpFoundation\Request;

class ValueObjectParamConverterTest extends TestCase
{
    /** @var ValueObjectParamConverter */
    private $paramConverter;

    public function setUp(): void
    {
        $this->paramConverter = new ValueObjectParamConverter();
    }

    /**
     * @param null|string $className
     * @param null|string $converter
     * @param bool        $expects
     *
     * @dataProvider supportsDataProvider
     */
    public function testSupports(?string $className, ?string $converter, bool $expects)
    {
        $configuration = new ParamConverter(['class' => $className, 'converter' => $converter]);

        $this->assertEquals($expects, $this->paramConverter->supports($configuration));
    }

    public function supportsDataProvider(): array
    {
        return [
            [null, null, false],
            ['', null, false],
            ['class', null, false],

            [null, 'androidlista.value_object', false],
            ['', 'androidlista.value_object', false],
            ['class', 'androidlista.value_object', true],

            ['class', 'some', false],
        ];
    }

    public function testApplyCorrect()
    {
        $request = new Request([], [], ['id' => 1]);

        $config = new ParamConverter([
            'class' => UserId::class,
            'name' => 'id',
            'converter' => 'androidlista.value_object'
        ]);

        $this->paramConverter->apply($request, $config);

        $this->assertInstanceOf(UserId::class, $request->attributes->get('id'));
        $this->assertEquals(1, $request->attributes->get('id')->value());
    }

    public function testApplyWrongClass()
    {
        $request = new Request([], [], ['id' => 1]);

        $config = new ParamConverter([
            'class' => 'wrongClass',
            'name' => 'id',
            'converter' => 'androidlista.value_object'
        ]);

        $this->expectException(\InvalidArgumentException::class);
        $this->paramConverter->apply($request, $config);
    }

    public function testApplyWrongMethod()
    {
        $request = new Request([], [], ['id' => 1]);

        $config = new ParamConverter([
            'class' => UserId::class,
            'name' => 'id',
            'converter' => 'androidlista.value_object',
            'options' => ['method' => 'wrongMethod']
        ]);

        $this->expectException(\InvalidArgumentException::class);
        $this->paramConverter->apply($request, $config);
    }
}
