<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter\Model;

use JMS\Serializer\Annotation as JMS;

class QueryRequestModel
{
    /**
     * @JMS\Type("string")
     */
    public $common;
    /**
     * @JMS\Type("string")
     */
    public $query;
    /**
     * @JMS\Type("string")
     */
    public $request;
}
