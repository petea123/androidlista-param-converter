<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter\Model;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class ValidatableModel
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    public $name;
}
