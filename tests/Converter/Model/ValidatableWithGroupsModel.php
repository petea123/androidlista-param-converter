<?php declare(strict_types=1);

namespace Androidlista\ParamConverterBundle\Tests\Converter\Model;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class ValidatableWithGroupsModel
{
    /**
     * @Assert\NotBlank(groups={"group1"})
     * @JMS\Type("string")
     */
    public $name;

    /**
     * @Assert\NotBlank(groups={"group2"})
     * @JMS\Type("string")
     */
    public $email;
}
