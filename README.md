# ParamConverterBundle

Bundle contains request parameters converters classes to cover the needs of delivering request and URL parameters
to controllers actions as models and value objects instead of arrays and raw intergers and strings.
The bundle use the symfony extra framework bundle [@ParamConverter mechanism](https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html).

----

## Installation

Require package: 

```console
composer require androidlista/param-converter-bundle
```

## Request DTO param converter

The `androidlista.request_dto` param converter allows to retrieve GET and POST parameters data into DTO model using [JMS serializer](https://jmsyst.com/libs/serializer/master/reference/annotations)
to convert array data to model object and Symfony Validator to validate the data when it gets to the object.

To cover the GET and POST (PUT, PATCH etc) cases param converter currently merges the GET parameters with POST body parameters
and gives priority to GET data. Consider the following example:

```
POST /me?limit=1&page=2

limit=4
name=something
```

The merged data to apply to the DTO will be:
```
limit=1
name=something
page=2
```

The following example shows how to use the param convertor in controller action annotation:

```php
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class RequestModel
{
    /**
     * @Assert\NotBlank()
     * @JMS\Type("string")
     */
    public $name;
}
```

We define a model class to convert request data to with serialization and validation rules in annotation.
It also worth to mention that model serialization and validation rules can be also configured using YAML and XML formats
using the corresponding library capabilities.

```php
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SomeController exctends Controller
{
    /**
     * @ParamConverter("model", converter="androidlista.request_dto")
     */
    public function postAction(RequestModel $model)
    {
        //
    }
}
```

We define some controller action and want to get all GET and POST data to a single model that is configured previously
to correctly get and validate request data. Model parameter in controller method signature can take any place.

The @ParamConverter annotation with `converter="androidlista.request_dto"` parameter tells symfony to search for corresponding
registered param converter to prepare `$model` method parameter as given in first annotaion parameter `"model"`.

In given case the param converter logic will run before the controller method code and in case the validation error appear
by default will be thrown `Androidlista\ParamConverterBundle\Exception\ParamConverterValidationException` exception. This exception
contains the Symfony validator `ConstraintViolationList` object and can be handled on a project level using the
symfony event listener subscribed to `kernel.exception` event.

There are a few additional options that can change the `androidlista.request_dto` param converter behavior:

```php
 /**
  * @ParamConverter("model", converter="androidlista.request_dto", options={"throw_exception":false})
  */
 public function postAction(RequestModel $model, ConstraintViolationList $violationsList)
 {
     if (count($violationsList)) {
        // Do something with errors
     }}
 }
```

If `throw_exception` if set to `false` than in case of validation error no exception will be thrown
and `ConstraintViolationList` object will be attached to request attribute `violationsList` and can be
retrieved in controller method as a separate parameter as shown in the above example.

Another option is validation groups to use in validator configuration. For instance:

```php
 /**
  * @ParamConverter("model", converter="androidlista.request_dto", options={"validator_groups":{"group1", "group2"}})
  */
 public function postAction(RequestModel $model)
 {
     //
 }
```

The models are free and not restricted to any interface or abstracts. Basically, any model can be used.

To cover the common case for getting list with pagination request like cgetAction(), where typically the parameters 
`limit`, `page`, `sort`, `sort_type` and `filters` are used bundle introduces the default abstract model class
which can be used: `Androidlista\ParamConverterBundle\Model\Request\AbstractListRequestModel`

---

## Value objects param converter

The `Androidlista.request_dto` param converter allows to convert URL parameters from raw int or string values to value objects
on the fly and get already converted object as a controller method parameter. Consider the following example:

```php
class UserId
{
    /** @var int */
    public $id;

    public static function existing(int $id)
    {
        return new self($id);
    }

    private function __construct($id)
    {
        $this->id = $id;
    }

    public function value()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        return (string) $this->id;
    }
}
```

We defined a value object that reperesents ID property of a user and has a private constructor. To convert a user id to
this object in controller we can use the following code:

```php
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SomeController exctends Controller
{
    /**
     * @FOS\Get("/users/{id}")
     * @ParamConverter("id", converter="androidlista.value_object")
     */
    public function getUserAction(UserId $id)
    {
        var_dump($id->value());
        var_dump((string) $id);
    }
}
```

Here we use a param converter `androidlista.value_object` for `"id"` value that will be implemented for `$id` controller
method parameter. This param converter will take `{id}` segment from URL string will call `UserId::existing($id)` method
with URL segment value and will attach the result UserId value object to controller method `$id` parameter.

The `androidlista.request_dto` param converter use by default the `existing` value object static method to create value objects.
The factory method name can be configured using param converter options, like in the following example: 

```php
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SomeController exctends Controller
{
    /**
     * @FOS\Get("/users/{id}")
     * @ParamConverter("id", converter="Androidlista.value_object", options={"method":"customMethodName"})
     */
    public function getUserAction(UserId $id)
    {
        //
    }
}
```

In this case the `UserId::customMethodName($id)` will be called to create a new value object.

Creation value object using its constructor is not supported right now.

---

## File Converter
The `androidlista.file` converter allows to inject uploaded files into method like any Symfony service. 
Consider the following example:

```php
/**
  * @ParamConverter("image", converter="androidlista.file"})
  */
 public function postAction(UploadedFile $image)
 {
     //
 }
```

Configuration allows to explicitly set a name of the file using  the `file_name` option:

```php
/**
  * @ParamConverter("image", converter="androidlista.file", options={"file_name":"my_image"})
  */
 public function postAction(UploadedFile $image)
 {
     //
 }
```

Note that using converter automatically makes your file mandatory, you will get an exception if is null or invalid. 
