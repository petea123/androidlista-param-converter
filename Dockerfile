FROM php:7.3.10-stretch
ENV LANG=C.UTF-8
RUN apt-get update && apt-get install -y \
    unzip \
    wget \
    procps && \
    apt-get clean

COPY docker/php/install-composer.sh /usr/local/bin/docker-app-install-composer
RUN chmod +x /usr/local/bin/docker-app-install-composer && \
    docker-app-install-composer && \
    mv composer.phar /usr/local/bin/composer

WORKDIR /srv/app/
CMD /usr/local/bin/php -S localhost:8000
